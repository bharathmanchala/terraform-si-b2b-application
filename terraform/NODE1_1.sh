#!/bin/bash
source NODE1_1.config
			
shareloc=$file/IMShared
siloc=$file/SterlingIntegrator
b2brepo=$file/Common_Repo/b2birepo
d=$(date +"%m-%d-%y")

sdk() {
case $1 in

1) nohup tar -xvf ibm-java-sdk-8.0-5.35-linux-ppc64le.tgz >>$file/log/preinstall_logs.txt
;;
2) nohup tar -xvf ibm-java-sdk-8.0-5.40-linux-x86_64.tgz >>$file/log/preinstall_logs.txt
;;
3) nohup start /d ibm-java-sdk-80-win-x86_64.exe >>$file/log/preinstall_logs.txt
;;
4) nohup tar -xvf j864redist.tar.gz >>$file/log/preinstall_logs.txt
;;
esac
}

im(){

case $1 in
1) nohup unzip IM_AIX.zip >>$file/log/preinstall_logs.txt
;;
2) nohup unzip IM_LinuxPPC64LE.zip >>$file/log/preinstall_logs.txt
;;
3) nohup unzip IM_LinuxPPC64LE.zip >>$file/log/preinstall_logs.txt
;;
4) nohup unzip IM_Linux.zip >>$file/log/preinstall_logs.txt
;;
5) nohup unzip IM_Win.zip >>$file/log/preinstall_logs.txt
esac
}

result=0
cd $file/ 
if [ "$media.zip" ]; then
    if [ ! -d "$media/" ]; then
    nohup unzip "$media.zip" -d "$file/$media" >>$file/log/preinstall_logs.txt
    sudo chmod -R 775 $file/$media 
    cd $media/InstallationManager/ 
    im $imenv
    IM="$(for i in $(ls -d */); do echo "${i%%/}"; done )"
    fi
else
    if [ -d "$media/" ]; then
    echo "Already Exist">>"$file"/log/preinstall_logs.txt
    cd $file/$media/InstallationManager/ 
    IM="$(for i in $(ls -d */); do echo "${i%%/}"; done )"
    else
    result=1
    echo "$media.zip file not exist or Envirnoment is not properly unzipped" >>"$file"/log/preinstall_logs.txt
    fi
fi

cd $file/ 
common=$media/Common_Repo.zip
if [ $common ]; then
    if [ ! -d "Common_Repo/" ]; then
    nohup unzip "$media/Common_Repo.zip" -d "$file/Common_Repo" >>$file/log/preinstall_logs.txt
    fi
else
    if [ -d "Common_Repo/" ]; then
    echo "Already Exist" >>$file/log/preinstall_logs.txt
    else
    echo "Common_repo.zip file does not exist" >>$file/log/preinstall_logs.txt
    result=1
    fi
fi

if [ "$sdk.zip" ]; then
    if [ ! -d "$sdk/" ]; then
    nohup unzip "$sdk.zip" -d "$file/$sdk" >>$file/log/preinstall_logs.txt
    sudo chmod -R 775 $file/$sdk
    cd $sdk/ 
    sdk $sdkenv
    SDK="$(for i in $(ls -d */); do echo "${i%%/}"; done )"
    fi
else
    if [ -d "$sdk/" ]; then
    echo "ALready Exist">>$file/log/preinstall_logs.txt
    cd $file/$sdk/ 
    SDK="$(for i in $(ls -d */); do echo "${i%%/}"; done )"
    else
    echo "$sdk.zip File doesn't exist" >>$file/log/preinstall_logs.txt
    result=1
    fi
fi

if [ $dbvendor == 'oracle' ];then
    cd $file/$sdk/ 
    SDK="$(for i in $(ls -d */); do echo "${i%%/}"; done )"
    cp -R $file/$jar $file/$sdk/$SDK/jre/bin
    cd $file/$sdk/$SDK/jre/bin
        echo -e "
    import java.util.*;
    import java.sql.*;

    public class OracleTest {

    public static void main(String[] args) {
        
        try
        {
            OracleTest oracleTest = new OracleTest();
            
            Connection conn = oracleTest.getOracleConnection();

            conn.close();
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public Connection getOracleConnection()
    {
        Connection ret = null;
        
        try
        {
            Class.forName(\"oracle.jdbc.driver.OracleDriver\");
            
            String connUrl = \"jdbc:oracle:thin:@$dbhost:$dbport:$dbdata\";
            String userName = \"$dbuser\";
            
            String password = \"$dbpassword\";
            
            ret = DriverManager.getConnection(connUrl, userName , password);
            
            DatabaseMetaData dbmd = ret.getMetaData();
            
            String dbName = dbmd.getDatabaseProductName();
            
            String dbVersion = dbmd.getDatabaseProductVersion();
            
            String dbUrl = dbmd.getURL();
            
            String uName = dbmd.getUserName();
            
            String driverName = dbmd.getDriverName();
            
            System.out.println(\"Database Name is \" + dbName);
            
            System.out.println(\"Database Version is \" + dbVersion);
            
            System.out.println(\"Database Connection Url is \" + dbUrl);
            
            System.out.println(\"Database User Name is \" + uName);
            
            System.out.println(\"Database Driver Name is \" + driverName);
            
            System.out.println(\"Database_was_Sucessfully_Connected\");
            
                        
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }finally
        {
            return ret;
        }
    }

    }" >>OracleTest.java

    chmod 775 OracleTest.java

    cd $file/$sdk/$SDK/bin
    ./javac ../jre/bin/OracleTest.java
    cd $file/$sdk/$SDK/jre/bin
   echo "Pragma@007" | sudo chmod 775 OracleTest.class
    ./java -classpath .:$jar OracleTest
    nohup ./java -classpath .:$jar OracleTest > dbtestresult.txt
    rm -rf OracleTest.class OracleTest.java 


    elif [ $dbvendor == 'MSSQL2005' ] || [ $dbvendor == 'MSSQL2012' ];then
    cd $file/$sdk/ 
    SDK="$(for i in $(ls -d */); do echo "${i%%/}"; done )"
    cp -R $file/$jar $file/$sdk/$SDK/jre/bin
    cd $file/$sdk/$SDK/jre/bin
        echo -e "
    import java.util.*;
    import java.sql.*;

    public class MSSQLTest {

    public static void main(String[] args) {
        
        try
        {
            MSSQLTest mssqlTest = new MSSQLTest();
            
            Connection conn = mssqlTest.getOracleConnection();

            conn.close();
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public Connection getOracleConnection()
    {
        Connection ret = null;
        
        try
        {
            Class.forName(\"com.microsoft.sqlserver.jdbc.SQLServerDriver\");
            
            String connUrl = \"jdbc:sqlserver://$dbhost:$dbport;databaseName=$dbdata\";
            String userName = \"$dbuser\";
            
            String password = \"$dbpassword\";
            
            ret = DriverManager.getConnection(connUrl, userName , password);
            
            DatabaseMetaData dbmd = ret.getMetaData();
            
            String dbName = dbmd.getDatabaseProductName();
            
            String dbVersion = dbmd.getDatabaseProductVersion();
            
            String dbUrl = dbmd.getURL();
            
            String uName = dbmd.getUserName();
            
            String driverName = dbmd.getDriverName();
            
            System.out.println(\"Database Name is \" + dbName);
            
            System.out.println(\"Database Version is \" + dbVersion);
            
            System.out.println(\"Database Connection Url is \" + dbUrl);
            
            System.out.println(\"Database User Name is \" + uName);
            
            System.out.println(\"Database Driver Name is \" + driverName);
            
            System.out.println(\"Database_was_Sucessfully_Connected\");
            
                        
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }finally
        {
            return ret;
        }
    }

    }">>MSSQLTest.java
    chmod 775 MSSQLTest.java

    cd $file/$sdk/$SDK/bin
    ./javac ../jre/bin/MSSQLTest.java
    cd $file/$sdk/$SDK/jre/bin
   echo "Pragma@007" | sudo chmod 775 MSSQLTest.class
    ./java -classpath .:$jar MSSQLTest
    nohup ./java -classpath .:$jar MSSQLTest > dbtestresult.txt
    rm -rf MSSQLTest.class MSSQLTest.java
   # c=0
    c=$(cat dbtestresult.txt | grep -c Database_was_Sucessfully_Connected)
fi
cd $file/$sdk/$SDK/jre/bin
c=$(cat dbtestresult.txt | grep -c Database_was_Sucessfully_Connected)

cd $file/$media/InstallationManager
IM="$(for i in $(ls -d */); do echo "${i%%/}"; done )"
if [ $result == 0 ] && [ $c == 1 ]
    then
    mkdir -p $install
    if [ ! -d $install/eclipse ]; then
        
        cd $file/$media/InstallationManager/$IM/ 
        nohup ./userinstc -installationDirectory $install -acceptLicense >>$file/log/preinstall_logs.txt
        cd $install/eclipse/tools/ 
        encryptStringdb=$(./imutilsc encryptString $dbpassword)
        encryptStringuser=$(./imutilsc encryptString $sipassword)
        else 
        if [ -d $install/eclipse ]; then
        echo "Installation manager already exist"
        else
        echo "the userinstc command is not found to install the installation manager or imutilsc file doesn't exist to encrypt the file"
        result=1
        fi

    fi
else

    echo "the files was not properly installed Or Already exist" >>$file/log/preinstall_logs.txt
    result=1
fi

if telnet -c $dbhost $dbport </dev/null 2>&1 | grep -q Escape; then
    connectP='success'
else
    connectP='no'
fi
cd $file/$sdk/$SDK/jre/bin
c=$(cat dbtestresult.txt | grep -c Database_was_Sucessfully_Connected)

if [ "$connectP" = "success" ] && [ $result == 0 ]  && [ $c -eq 1 ]
    then 
    echo -e "<?xml version='1.0' encoding='UTF-8'?> 
    <!-- 
    IBM Sterling B2B Integrator 
    Licensed Materials - Property of IBM 
    (C) Copyright IBM Corporation and its licensors 2013, 2014, 2015. All Rights Reserved. 
    US Government Users Restricted Rights - Use, duplication, or disclosure restricted by GSA ADP 
    Schedule Contract with IBM Corp.
    IBM and the IBM logo are trademarks of IBM Corporation in the United States, other countries, or 
    both.  Java and all Java-based trademarks and logos are trademarks or registered trademarks of 
    Oracle and/or its affiliates. Other company, product or service names may be trademarks or 
    service marks of others. 
    --> 
    <!-- 
    Use this file to install IBM Sterling B2B Integrator. 
    How to use this file 
        1. Replace values that must be updated to match your environment and your selected installation 
        option. Variables 
        enclosed in \$$ must be changed. Input to certain password fields must be encrypted using the 
        method described in the \"Protecting Passwords\" section below. 
        2. Using the descriptions for values in this file, confirm provided default values are 
        acceptable for your environment.  
        3. Obtain the information specific to your database for B2Bi. 
        6. After modifying this sample response file according to your installation environment 
        details, start installation and provide this file as input, i.e., 
        <iim_install_dir>/InstallationManager/eclipse/tools/imcl -input Responsefile_B2Bi_GM.xml -acceptLicense 
    Protecting Passwords 
        Passwords must be provided with script to decrypt. For example: 
        -bash-3.2$ ./imutilsc encryptString Password 
        NnO7aEXCW36ozr3feBXWTQ== 
        -bash-3.2$ pwd 
        /fullpath/IBM/InstallationManager/eclipse/tools 
        more details in the \"Installation Manager command-line arguments for silent mode\" topic, at 
        https://www-01.ibm.com/support/knowledgecenter/api/content/nl/en-us/SSDV2W_1.8.2/com.ibm.silentinstall12.doc/topics/r_silent_inst_cmd_arg.html 

    --> 

    <agent-input> 
    <install modify='false'> 
        <!-- IBM B2B Sterling Integrator --> 
        <!-- To select other optional B2Bi features, add the feature to the list of features to 
            install. For example, 
            features='main.feature,filegateway.feature,fips.feature,as2.feature,financial.feature,ebics.feature,meigIntegration.feature' --> 
        <offering profile='Sterling Integrator' id='com.ibm.sterling.integrator.install.offering' features='main.feature' installFixes='none'/>  
    </install> 

    <variables> 
        <!-- 
        Define the variable for the location for the Installation Manager package group. 
        --> 
        <variable name='sharedLocation' value='$shareloc'/> 

        <!-- 
        Define the variable (siLocation) to use for your B2Bi installation. If you are installing  
        only B2Bi, ensure this directory does not exist. Ensure that the parent directory/path does 
        exist prior to installing. 
        If you are installing GM at the same time, GM must be installed in this same directory. 
        When installing a node with GM only, this directory must be the same as its associated B2Bi 
        instance. 
        --> 
        <variable name='siLocation' value='$siloc'/> 
    </variables> 
    <server> 
        <!-- 
        These are the repositories where Installation Manager can find offerings. 
        Provide the full path to the location of the B2Bi/GM repository, which is within your 
        installation package. This should be in the same installation package that contained this 
        sample file. 

        --> 
        <!-- If you are installing GM only, comment out or delete this element indicating the B2Bi repository location.--> 
        <repository location='$b2brepo'/> 
    </server> 

    <!-- 
    The location of the install is set in the <variables> section, earlier in this file. Once 
    defined, you may reuse it throughout this file. 
    --> 
    <profile id='Sterling Integrator' installLocation='\${siLocation}'> 
        <data key='eclipseLocation' value='\${siLocation}'/> 
        <!-- 
        These values are needed for Installation Manager. Leave defaults.	--> 
        <data key='user.import.profile' value='false'/> 
        <data key='cic.selector.os' value='linux'/> 
        <data key='cic.selector.arch' value='x86_64'/> 
        <data key='cic.selector.ws' value='gtk'/> 
        <data key='cic.selector.nl' value='en'/> 

        <!--The install parameters below are specific to SI installation --> 
        <!-- 
        Provide the full path to the location where you downloaded the jdk provided in the 
        installation package. 	
        Value=Full path/jdk downloaded location--> 
        <data key='user.sb.JVM_LOC,com.ibm.sterling.integrator.install.offering' value='$file/$sdk/$SDK'/> 
        <data key='user.sb.JDK64BIT,com.ibm.sterling.integrator.install.offering' value='true'/> 
        <!-- 
        FIPS mode should be set to value=true if you are enabling FIPS. Otherwise, leave default. --> 
        <data key='user.sb.FIPS_MODE,com.ibm.sterling.integrator.install.offering' value='false'/> 
        <!-- 
        If you are enabling NIST mode set to value=transition or value=strict, depending on the 
        level you require. Otherwise, leave default value=off. -->	
        <data key='user.sb.NIST_MODE,com.ibm.sterling.integrator.install.offering' value='off'/> 
        <!-- 
        Set to value=true to enable SPE integration, otherwise set to false. -->	
        <data key='user.sb.INSTALL_SPE_INTEGRATION,com.ibm.sterling.integrator.install.offering' value='false'/> 

        <!-- If you intend to use SPE integration, uncomment the below SPE/WTX lines and set the 
        below values. --> 
        <!-- 
        <data key='user.sb.SPE_INSTALL_DIR,com.ibm.sterling.integrator.install.offering' value='\$$/spepath/spedirname\$$'/> 
        <data key='user.sb.SPE_UI_PORT,com.ibm.sterling.integrator.install.offering' value='9080'/> 
        --> 
        <!--The WTX install directory is optional for SPE integration. After uncommenting, clear 
        the value if you do not wish to integrate B2Bi with WTX. --> 
        <!-- 
        <data key='user.sb.WTX_INSTALL_DIR,com.ibm.sterling.integrator.install.offering' value='\$$/wtxpath/wtxdirname\$$'/> 
        --> 

        <!-- 
        Due to regulatory restrictions, JCE files must be obtained separately. JCE files for IBM 
        SDKs are available for download at 
        https://www-01.ibm.com/marketing/iwm/iwm/web/preLogin.do?source=jcesdk  
        If you are using an non-IBM SDK, obtain the JCE files from your SDK vendor.  
        --> 
        <data key='user.sb.JCE_DIST_FILE,com.ibm.sterling.integrator.install.offering' value='$file/$jce'/> 

        <!-- 
        Value=IP address or host name of the system on which you are now installing. --> 
        <data key='user.sb.INSTALL_IP,com.ibm.sterling.integrator.install.offering' value='$sihost'/> 
        <!-- 
        Value=Number of Sterling B2B Integrator initial port; requires a series of 200 available 
        ports, starting with the port selected for this value 
        Example: value='33700' --> 
        <data key='user.sb.PORT1,com.ibm.sterling.integrator.install.offering' value='$siport'/> 

        <!-- 
        System passphrase information needed for Sterling B2B Integrator. See section in this file 
        for handling encrypted passphrase.  
        --> 
        <data key='user.sb.APSERVER_PASS,com.ibm.sterling.integrator.install.offering' value='$encryptStringuser'/> 
        <data key='user.confirmPassphrase,com.ibm.sterling.integrator.install.offering' value='$encryptStringuser'/> 

        <!-- 
        Value=Email address to which Sterling B2B Integrator system level notifications should be 
        sent.   -->  
        <data key='user.sb.SI_ADMIN_MAIL_ADDR,com.ibm.sterling.integrator.install.offering' value='$smtpmail'/> 
        <!-- 
        Value=Port of the SMTP host for the notification email address.   --> 
        <data key='user.sb.SI_ADMIN_SMTP_HOST,com.ibm.sterling.integrator.install.offering' value='$smtphost'/> 

        <!-- 
        Database vendor information. Example file shows non-clustered DB2. See 
        \"Getting Started > System Requirements > Databases\" topic in IBM Knowledge Center for 
        Sterling B2B Integrator - 
        http://www-01.ibm.com/support/knowledgecenter/SS3JSW_5.2.0/welcome/product_landing.html - 
        for the values specific to clustered DBs and other vendors. Have these values pre-determined 
        and entered on your install check list for reference here. 

        Note: Encrypted passwords require a script to decrypt protected values. See instruction in 
        the Protecting PASSWORDS section. 
        --> 
        <data key='user.sb.DB_VENDOR,com.ibm.sterling.integrator.install.offering' value='$dbvendor'/> 
        <!-- 
        Leave defaults for non-clustered B2Bi instances. 
        For clustered B2Bi instances, leave defaults for the initial node. For node 2 and higher, 
        set user.sb.CLUSTER to true and give the cluster node number (starting sequentially from 2) 
        in the user.CLUSTER_NODE_NUM field. 
        --> 
        <data key='user.sb.CLUSTER,com.ibm.sterling.integrator.install.offering' value='false'/> 
        <data key='user.CLUSTER_NODE_NUM,com.ibm.sterling.integrator.install.offering' value=''/> 
        <!-- 
        Create schema automatically, unless you are installing successive nodes in a clustered 
        B2Bi. If installing second or successive nodes in a B2Bi cluster, this MUST be set to false.  
        --> 
        <data key='user.sb.DB_CREATE_SCHEMA,com.ibm.sterling.integrator.install.offering' value='true'/>     
        <data key='user.sb.DB_USER,com.ibm.sterling.integrator.install.offering' value='$dbuser'/> 
        <!--The values of the DB_PASS and dbconfirmPassword fields must be encrypted. See 
        the \"Protecting Passwords\" section earlier in this file for details. --> 
        <data key='user.sb.DB_PASS,com.ibm.sterling.integrator.install.offering' value='$encryptStringdb'/> 
        <data key='user.dbconfirmPassword,com.ibm.sterling.integrator.install.offering' value='$encryptStringdb'/> 
        <data key='user.sb.DB_DATA,com.ibm.sterling.integrator.install.offering' value='$dbdata'/> 
        <data key='user.sb.DB_HOST,com.ibm.sterling.integrator.install.offering' value='$dbhost'/> 
        <data key='user.sb.DB_PORT,com.ibm.sterling.integrator.install.offering' value='$dbport'/> 
        <data key='user.sb.DB_DRIVERS,com.ibm.sterling.integrator.install.offering' value='$file/$jar'/> 

        <!--License file required for installation. Leave default. --> 
        <data key='user.sb.LICENSE_FILE,com.ibm.sterling.integrator.install.offering' value='Core_License.xml'/> 

        <!-- 
        B2Bi Optional Settings. 
        Set debug value=true for verbose logging. 
        Set upgrade value=true to use an existing database. 
        Accept defaults for number of processors and memory to allocate; these values can be 
        adjusted after installation using tuning.properties. 
        --> 	
        <data key='user.sb.DEBUG,com.ibm.sterling.integrator.install.offering' value='true'/> 
        <data key='user.sb.UPGRADE,com.ibm.sterling.integrator.install.offering' value='false'/> 
        <data key='user.sb.PROCESSORS,com.ibm.sterling.integrator.install.offering' value='$core'/> 
        <data key='user.sb.MEMORY,com.ibm.sterling.integrator.install.offering' value='$ram'/> 
    </profile> 

    <!-- 
    Installation Manager specific values. 
    Leave all defaults.   --> 	 

    <preference name='com.ibm.cic.common.core.preferences.eclipseCache' value='\${sharedLocation}'/> 
    <preference name='com.ibm.cic.common.core.preferences.connectTimeout' value='30'/> 
    <preference name='com.ibm.cic.common.core.preferences.readTimeout' value='45'/> 
    <preference name='com.ibm.cic.common.core.preferences.downloadAutoRetryCount' value='0'/> 
    <preference name='offering.service.repositories.areUsed' value='true'/> 
    <preference name='com.ibm.cic.common.core.preferences.ssl.nonsecureMode' value='false'/> 
    <preference name='com.ibm.cic.common.core.preferences.http.disablePreemptiveAuthentication' value='false'/> 
    <preference name='http.ntlm.auth.kind' value='NTLM'/> 
    <preference name='http.ntlm.auth.enableIntegrated.win32' value='true'/> 
    <preference name='com.ibm.cic.common.core.preferences.preserveDownloadedArtifacts' value='true'/> 
    <preference name='com.ibm.cic.common.core.preferences.keepFetchedFiles' value='false'/> 
    <preference name='PassportAdvantageIsEnabled' value='false'/> 
    <preference name='com.ibm.cic.common.core.preferences.searchForUpdates' value='false'/> 
    <preference name='com.ibm.cic.agent.ui.displayInternalVersion' value='false'/> 
    <preference name='com.ibm.cic.common.sharedUI.showErrorLog' value='true'/> 
    <preference name='com.ibm.cic.common.sharedUI.showWarningLog' value='true'/> 
    <preference name='com.ibm.cic.common.sharedUI.showNoteLog' value='true'/> 
    </agent-input>" > $file/$media/ResponseFiles/SampleResponseFiles/responseFile_B2Bi_$d.xml

    cd $file/$media/ResponseFiles/SampleResponseFiles/ 
    chmod 775 responseFile_B2Bi_"$d".xml

    cd $install/eclipse/tools/ 

    export LANG=en_US

    nohup ./imcl input $file/$media/ResponseFiles/SampleResponseFiles/responseFile_B2Bi_$d.xml -acceptLicense -showVerboseProgress -log $file/logs/si603install.log >>$file/log/logs.txt

    checks=$(grep -c ~/var/ibm/InstallationManager/logs/*.log | wc -l)
    check=$(find ~/var/ibm/InstallationManager/logs/*.log -type f -mmin -30)
    if [ "$checks" -gt 0 ]; then
        sucess=$(cat "$check" | grep -c "Deployer completed successfully")
        if [ "$sucess" == 3 ] 
        then
            cd $file 
            mkdir si_packages
            mv $sdk/ si_packages/
            mv $sdk.zip si_packages/
            mv $media/ si_packages/
            mv $jce $jar si_packages/
            mv $media.zip si_packages/
            mv Common_Repo/ si_packages/
            cd $siloc/bin
            else
            echo "Installation is failed">>$file/log/logs.txt
        fi
     else
        if [ -f $file/$media/ResponseFiles/SampleResponseFiles/responseFile_B2Bi_$d.xml ]; then
            cd $install/eclipse/tools/ 
            export LANG=en_US
            nohup ./imcl input $file/$media/ResponseFiles/SampleResponseFiles/responseFile_B2Bi_$d.xml -acceptLicense -showVerboseProgress -log $file/logs/si603install.log >>$file/log/logs.txt
            else
            echo "Installation is failed">>$file/log/logs.txt
        fi
    fi
    else
    echo "The Database Cann't able to connect please check and enter once again" >>$file/log/logs.txt
fi
